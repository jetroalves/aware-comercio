
import java.io.BufferedReader;
import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.filechooser.FileNameExtensionFilter;

public class Program {

	public static void main(String[] args) {

		File[] files;
//		do {
		files = buscarArquivosCsv();
//		} while (files == null); 

//		for (File file : files) {
//			System.out.println(file.getPath());
//		}

		if (files != null) {
			for (int i = 0; i < files.length; i++) {

				try (BufferedReader br = new BufferedReader(new FileReader(files[i]))) {

					String fileValido = "VALIDADO";
					String fileInvalido = "INVALIDADO";
					File path = new File(files[i].getPath().toString());
					String line = br.readLine();

					if (line == null) {
						new File(path.getParent() + "\\" + fileInvalido).mkdir();
//					System.out.println("Invalido!");
						InputStream input = new FileInputStream(files[i]);
						OutputStream out = new FileOutputStream(
								path.getParent() + "\\" + fileInvalido + "\\" + path.getName());

						// Copy the bits from instream to outstream
						byte[] buf = new byte[1024];
						int len;
						while ((len = input.read(buf)) > 0) {
							out.write(buf, 0, len);
						}
						input.close();
						out.close();

					} else if (line != null) {

						String[] fields1 = line.split(";");

						if (fields1.length != 4) {

							new File(path.getParent() + "\\" + fileInvalido).mkdir();
//							System.out.println("Invalido!");
							InputStream input = new FileInputStream(files[i]);
							OutputStream out = new FileOutputStream(
									path.getParent() + "\\" + fileInvalido + "\\" + path.getName());

							// Copy the bits from instream to outstream
							byte[] buf = new byte[1024];
							int len;
							while ((len = input.read(buf)) > 0) {
								out.write(buf, 0, len);
							}
							input.close();
							out.close();

						} else {

//							System.out.println(path.getParent());
							new File(path.getParent() + "\\" + fileValido).mkdir();
	//						System.out.println("OK!");
							InputStream input = new FileInputStream(files[i]);
							OutputStream out = new FileOutputStream(
									path.getParent() + "\\" + fileValido + "\\" + path.getName());

							// Copy the bits from instream to outstream
							byte[] buf = new byte[1024];
							int len;
							while ((len = input.read(buf)) > 0) {
								out.write(buf, 0, len);
							}
							input.close();
							out.close();

						}
					}

				} catch (IOException e) {
					System.out.println("Error: " + e.getMessage());
				}
			}
		}
	}

	public static File[] buscarArquivosCsv() {

		File[] pathCsv = null;

		JFileChooser pathChooser = new JFileChooser();
		pathChooser.setDialogTitle("Escolha o diretório que contenha o(s) arquivo(s) .csv ...");
		pathChooser.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);
		FileNameExtensionFilter filtro = new FileNameExtensionFilter("Selecionar Diretório", "csv");
		pathChooser.setFileFilter(filtro);
		int retorno = pathChooser.showOpenDialog(null);

		if (retorno == 0 && retorno == JFileChooser.APPROVE_OPTION) {
			File path = pathChooser.getSelectedFile();

			if (path.isDirectory()) {

				pathCsv = path.listFiles(new FileFilter() {
					public boolean accept(File file) {
						if (file.getName().endsWith(".csv")) {
							return true;
						}
						return false;
					}
				});

			} else {
				JOptionPane.showMessageDialog(null, new String("Selecione um diretório onde contem arquivo(s)(.csv)"));
			}
		}
		return pathCsv;
	}
}
